Node.js - Introduction
- what is node js and what is build upon ?
- node js is mainly build for ?
- note:  node.js = runtime environment + java script library 
- mention some features of node.js ?
    - Asynchronous and Event Driven
    - Single Threaded but Highly Scalable
    - No Buffering
    - Very Fast
- what do you mean by node.js Asynchronous and Event Driven ?
- what do you mean by node.js is Single Threaded but Highly Scalable ?
- what do you mean by node.js is No Buffering ?
- what do you mean by node.js is very fast ?
- where to use node.js ?
- whare not to use node.js ?

Node.js - Environment Setup
- which extention use for node.js file ?
- which interpreter will be use to interpret and execute your javascript code ?
- how to install nodejs on  unix/linux/mac/sun from binary ?
- how to install nodejs on window using binary ?
- how to verify installation of nodejs ?

Node.js - First Application
- what are the 3 main component of nodejs application?
- what is the require directive use for ?
- note: step 1 import required module 
        step 2 create serevr 
        step 2. testing the request and response 
- write a simple code as below 
    - import the http module 
    - create an http instance
    - create serevr 
    - liste to the port 8080 
    - pass a function with request and response in argument to the createSerevr()


Node.js - REPL Terminal
- what is REPL ?
- how to run the REPL ?
- how to do the below on REPL ?
    - use variable ?
    - multiline Expression  ?
    - underscore variable ?
- what is the underscore variable use for in REPL ?
- what are the commands avilable in REPL ?
    - ctrl + c 
    - ctrl + c twice
    - ctrl + d
    - Up/Down Keys
    - tab Keys
    - .help
    - .break
    - .clear
    - .save filename
    - .load filename

Node.js - NPM
- what does the npm do ?
- how to check which version npm installed on you pc ?
- how to update the npm ?
- how to install module using npm ?
- how to use installed module in the .js code ?
- how to install module locally using npm  ?
- how and where can we use the locally install module ?
- how to list Down all the locally installed module ?
- how and where can we use the globally  install module ?
- how to list Down all the globally installed module ?
- what is pacakage.json file ?
- what are all the Attributes of Package.json ?
    - name
    - version
    - description
    - homepage
    - author
    - contributors
    - dependencies
    - repository
    - main
    - keywords
- how to Uninstalling a Module ?
- how to Updating a Module ?
- how to Search a Module ?
- how to Create a Module ?
    - npm init 
    - npm adduser
    - npm publish

Node.js - Callbacks Concept
- What is Callback?
- write a simple file reading code in node js to show the blocking call ?
- write a simple file reading code in node js to show non blocking call ?

Node.js - Event Loop
https://www.tutorialspoint.com/nodejs/nodejs_event_loop.htm
- how nodejs works ?
- what is the diffrence b/t the event and callback ?
- note: the basic procudure for the event attachment with listner? 
        // Import events module
        var events = require('events');

        // Create an eventEmitter object
        var eventEmitter = new events.EventEmitter();

        // Bind event and event  handler as follows
        eventEmitter.on('eventName', eventHandler);

        // Fire an event 
        eventEmitter.emit('eventName');
- what is the last param of the asyn function in nodejs ?
- what is the first parm of callback funtion in nodejs ?

Node.js - Event Emitter
- what module whould we import for creating an event ?
- what is the class name  which is used to create an event instance ?
- what event occur when there is an error in the event ?
- what event occur when a new listenr is added ?
- what event occur when listner is removed ?
- what is the "on" property used for in the event ?
- what is the "emit" property used for in the event ?
- name some  method avilable in the eventEmitter ?
    - addListener(event, listener)
    - on(event, listener)
    - once(event, listener)
    - removeListener(event, listener)
    - removeAllListeners([event])
    - setMaxListeners(n)
    - listeners(event)
    - emit(event, [arg1], [arg2], [...])

    - listenerCount(emitter, event)

    - newListener(event, listener)
    - removeListener(event, listener)
    - listenerCount(eventEmitter,'<event_name>')


Node.js - Buffers
https://www.tutorialspoint.com/nodejs/nodejs_buffers.htm

- is pure javascript is Unicode friendly ?
- is pure javascript is binary data friendly ?
- how to dealing with TCP streams or the file system and octet streams ?
- which class is use to provide instance to store raw data into the array of integer ?
- is this possible to use  Buffer class without import ?
- create an uninitiated Buffer of 10 octets ?
- create a Buffer from a given array ?
- create a Buffer from a given string and optionally encoding ?
- how to Writing to Buffers ?
- note: syntax of the method to write into a Node Buffer
    - buf.write(string[, offset][, length][, encoding])
- how to Reading from Buffers ?
- note: syntax Reading from Buffers 
    - buf.toString([encoding][, start][, end])
- Convert Buffer to JSON ?
    - buf.toJSON()
- Concatenate Buffers ?
    - Buffer.concat([buffer1,buffer2]);
- Compare Buffers ?
    - buf.compare(otherBuffer); 
- Copy Buffer ?
    - buffer1.copy(buffer2);
- Slice Buffer ?
    - buffer1.slice(0,9);
- Buffer Length ?
    - buf.length;

Node.js - Streams
- What are Streams ?
- what do u mean by  Readable  stream  ?
- what do u mean by  Writable  stream  ?
- what do u mean by  Duplex  stream  ?
- what do u mean by  Transform  stream  ?
- what are the Streams related to stream ?
    - data
    - end
    - error
    - finish
- write a code to Reading from a Stream  with events data, end, error ?
    - createReadStream()
- write a code to Writing to a Stream  with events data, end, error ?   
    - createWriteStream()
- write a code to Piping the Streams  with events data, end, error ?   
- write a code to Chaining the Streams with events data, end, error ?   
    - fs.createReadStream('input.txt.gz')
        .pipe(zlib.createGunzip())
        .pipe(fs.createWriteStream('input.txt'));


Node.js - File System
- write a code to read .txt file Synchronously ?
- write a code to read .txt file ASynchronously ?
- what is the open() syntax ?
    - fs.open(path, flags[, mode], callback)
- what is path here ?
- what is flags here ?
- what is mode here ?
- what is callback here ?
- what does the mean below flags ?
    - r
    - r+
    - rs
    - rs+
    - w
    - wx
    - wx+
    - a
    - ax 
    - a+
    - ax+
- how to open a file ASynchronously ?
- how to get status of a file ?
- write a code to write data to a file and read ASynchronously ?
    - fs.writeFile(filename, data[, options], callback)
    - fs.readFile('input.txt', function (err, data)
- write a code to read from the file object and write to a buffer ? 
    - fs.read(fd, buffer, offset, length, position, callback)
- how to Closing a File ?
    - fs.close(fd, callback) ?
- Truncate a File ?
    - fs.ftruncate(fd, len, callback) ?
- Delete a File ?
    - fs.unlink(path, callback)
- Create a Directory ?
    - fs.mkdir(path[, mode], callback)
- Read a Directory  
    - fs.readdir(path, callback)
- Remove a Directory  
    - fs.rmdir(path, callback)

Node.js - Global Objects
- what is Global object ?
- __filename ?
- __dirname ?
- setTimeout(cb, ms) ?
- clearTimeout(t) ?
- setInterval(cb, ms) ?

Node.js - Utility Modules
- OS Module
- Path Module
- Net Module
- DNS Module
- Domain Module

Node.js - Express Framework
https://www.tutorialspoint.com/nodejs/nodejs_express_framework.htm
- what is express ?
- how to install  express locally ?
- what is body-parser and how to install   locally ?
- what is cookie-parser andhow to install   locally ?
- what is multer  and how to install   locally ?
- write a simple Hello world code using express ?
    - Express app which starts a server and listens on port 8081 for connection. This app responds with Hello World! for requests to the homepage.
- what is Request object and which property is contains?
    - query string, parameters, body, HTTP headers, and so on.
- what is Response object and which property is contains ?
- write a sample code to show the basic routing like GET, POST, and so on).
- how to Serving Static Files in express ?
    - app.use(express.static('public'));
- write a sample code to process_get in express including the html file ?
- write a sample code to process_post in express including html file ?
- write a sample code to process_post in express including html file and body-parser ?
- what to set enctype attribute   while file upload ?
- write a sample code to Cookies Management in express?

Node.js - RESTful API
- write a sample rest api to get list of user from the given json.file ?
- write a sample rest api to get user detail  from the given json.file ?
- write a sample rest api to add a user  from the given json.file ?
- write a sample rest api to delete a user  from the given json.file ?


Node.js - Scaling Application
- child processes to leverage parallel processing on multi-core CPU based systems.
    - child_process.exec(command[, options], callback)
    - child_process.spawn(command[, args][, options])
    - child_process.fork(modulePath[, args][, options])

Node.js - Packaging
- which module is use for pacakaging ?
    - wget https://s3.amazonaws.com/nodejx/jx_rh64.zip
    - unzip jx_rh64.zip
    - cp jx_rh64/jx /usr/bin

    - export PATH=$PATH:/usr/bin
    - jx --version

    - jx package index.js index
    - node index.js command_line_arguments